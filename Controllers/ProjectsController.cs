using jira_dashboard.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace jira_dashboard.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
       private readonly DatabaseContext _context; 

       public ProjectsController(DatabaseContext context)
       {
           _context = context;
       }
       
       [HttpGet] //https://localhost:5001/api/project/1 
       public async Task<ActionResult<IEnumerable<Project>>> GetProjects()
       {
           return await _context.Projects.ToListAsync();
       }

       [HttpGet("{id}")] //https://localhost:5001/api/project/1
       public async Task<ActionResult<Project>> GetProject(long id)
       {
           var project = await _context.Projects.FindAsync(id);

           if(project == null){
               return NotFound();
           }
           return project;
       }

       [HttpGet("{id}/tasks")]
        // https://localhost:5001/api/projects/1/tasks
        public async Task<ActionResult<IEnumerable<ProjectTask>>> GetProjectTasks(long id)
        {
            return await _context.ProjectTasks.Where(b => b.ProjectId == id).ToListAsync();
        }

       [HttpPost] //https://localhost:5001/api/project/
       public async Task<ActionResult<Project>> PostProject(Project project)
       {
            _context.Projects.Add(project);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProject", new { id = project.Id}, project);
       }

       [HttpDelete("{id}")]
       public async Task<ActionResult<Project>> DeleteProject(long id){
           var project = await _context.Projects.FindAsync(id);

           if(project == null){
               return NotFound();
           }
           _context.Projects.Remove(project);
           await _context.SaveChangesAsync();
           return project;
       }

        [HttpPut("{id}")]
       public async Task<ActionResult<Project>> UpdateProject(long id, Project project){
           if(id != project.Id){
               return BadRequest();
           }
           _context.Entry(project).State = EntityState.Modified;
           await _context.SaveChangesAsync();
           return CreatedAtAction("GetProject", new {id = project.Id}, project);
       }
    }
}