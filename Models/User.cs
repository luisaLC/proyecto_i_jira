namespace jira_dashboard.Models
{ 
    public class User 
    {
        public long Id {get; set;}

        //[Required(ErrorMessage = "Username is required.")]
        public string Username {get; set;}

        //[Required(ErrorMessage = "Name is required.")]
        public string Name {get; set;}

        //[Required(ErrorMessage = "Lastname is required.")]
        public string Lastname{get; set;}

        //[Required(ErrorMessage = "Password is required.")]
        public string Password {get; set;}

        //[Required(ErrorMessage = "Email is required.")]
        public string Email {get; set;} 
    }
}