using jira_dashboard.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace jira_dashboard.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectTasksController : ControllerBase
    {
       private readonly DatabaseContext _context; 

       public ProjectTasksController(DatabaseContext context)
       {
           _context = context;
       }
       
       [HttpGet] //https://localhost:5001/api/status_tasks/1
       public async Task<ActionResult<IEnumerable<ProjectTask>>> GetProjectTasks()
       {
           return await _context.ProjectTasks.ToListAsync();
       }

       [HttpGet("{id}")] //https://localhost:5001/api/status_tasks/1
       public async Task<ActionResult<ProjectTask>> GetProjectTask(long id)
       {
           var statusTask = await _context.ProjectTasks.FindAsync(id);

           if(statusTask == null){
               return NotFound();
           }
           return statusTask;
       }

       [HttpPost] //https://localhost:5001/api/status_tasks
       public async Task<ActionResult<ProjectTask>> PostProjectTask(ProjectTask statusTask)
       {
            _context.ProjectTasks.Add(statusTask);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProjectTask", new { id = statusTask.Id}, statusTask);
       }

       [HttpDelete("{id}")] //https://localhost:5001/api/status_tasks
       public async Task<ActionResult<ProjectTask>> DeleteTask(long id){
           var statusTask = await _context.ProjectTasks.FindAsync(id);

           if(statusTask == null){
               return NotFound();
           }
           _context.ProjectTasks.Remove(statusTask);
           await _context.SaveChangesAsync();
           return statusTask;
       }

        [HttpPut("{id}")]
       public async Task<ActionResult<ProjectTask>> UpdateProjectTask(long id, ProjectTask statusTask){
           if(id != statusTask.Id){
               return BadRequest();
           }
           _context.Entry(statusTask).State = EntityState.Modified;
           await _context.SaveChangesAsync();
           return CreatedAtAction("GetProjectTask", new {id = statusTask.Id}, statusTask);
       }
    }
}