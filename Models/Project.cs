using System.Collections.Generic;

namespace jira_dashboard.Models

{
    public class Project
    {
        public long Id {get; set;}
        
        //[Required(ErrorMessage = "Description is required.")]
        public string description {get; set;}

        public List<ProjectTask> ProjectTasks {get; set;}
    }
}