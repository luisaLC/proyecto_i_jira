namespace jira_dashboard.Models
{ 
    public class ProjectTask
    {
        public long Id {get; set;}

        //[Required(ErrorMessage = "Title is required.")]
        public string Title {get; set;}

        //[Required(ErrorMessage = "Description is required.")]
        public string Description {get; set;}

        public long ProjectId {get; set;}

        public Project Project {get; set;}

        public long StatusId {get; set;}

        public Status Status {get; set;}

        public long UserId {get; set;}

        public User User {get; set;}
    }
}