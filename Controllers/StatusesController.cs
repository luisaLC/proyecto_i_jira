using jira_dashboard.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace jira_dashboard.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatusesController : ControllerBase
    {
       private readonly DatabaseContext _context; 

       public StatusesController(DatabaseContext context)
       {
           _context = context;
       }
       
       [HttpGet] //https://localhost:5001/api/statuses/1 
       public async Task<ActionResult<IEnumerable<Status>>> GetStatuses()
       {
           return await _context.Statuses.ToListAsync();
       }

       [HttpGet("{id}")] //https://localhost:5001/api/statuses/1
       public async Task<ActionResult<Status>> GetStatus(long id)
       {
           var status = await _context.Statuses.FindAsync(id);

           if(status == null){
               return NotFound();
           }
           return status;
       }

       [HttpPost] //https://localhost:5001/api/statuses/
       public async Task<ActionResult<Status>> PostStatus(Status status)
       {
            _context.Statuses.Add(status);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStatus", new { id = status.Id}, status);
       }

       [HttpDelete("{id}")]
       public async Task<ActionResult<Status>> DeleteStatus(long id){
           var status = await _context.Statuses.FindAsync(id);

           if(status == null){
               return NotFound();
           }
           _context.Statuses.Remove(status);
           await _context.SaveChangesAsync();
           return status;
       }

        [HttpPut("{id}")]
       public async Task<ActionResult<Status>> UpdateStatus(long id, Status status){
           if(id != status.Id){
               return BadRequest();
           }
           _context.Entry(status).State = EntityState.Modified;
           await _context.SaveChangesAsync();
           return CreatedAtAction("GetStatus", new {id = status.Id}, status);
       }
    }
}