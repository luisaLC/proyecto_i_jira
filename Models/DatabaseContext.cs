using Microsoft.EntityFrameworkCore;

namespace jira_dashboard.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            
        }

        //tabla de Project
        public DbSet<Project> Projects {get; set;}

        //tabla de Status_task
        public DbSet<ProjectTask> ProjectTasks {get; set;}

        //tabla de Status
        public DbSet<Status> Statuses{get; set;}

        //tabla de User
        public DbSet<User> Users {get; set;}
    }
}